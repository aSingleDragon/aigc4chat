package pers.lys.aigc4chat.model.baidu.request.body;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;

/**
 * <a href="https://cloud.baidu.com/doc/WENXINWORKSHOP/s/4lilb2lpf">ERNIE-Lite-8K-0922</a>
 * 对话请求体
 *
 * @author hll
 * @since 2024/05/05
 */
@Data
public class ChatReqBody {

    /**
     * 聊天上下文信息
     * <p>
     * 说明：
     * <ol>
     *   <li>messages成员不能为空，1个成员表示单轮对话，多个成员表示多轮对话</li>
     *   <li>最后一个message为当前请求的信息，前面的message为历史对话信息</li>
     *   <li>必须为奇数个成员，成员中message的role必须依次为user、assistant</li>
     *   <li>message中的content总长度和system字段总内容不能超过11200个字符，且不能超过7168 tokens</li>
     * </ol>
     * </p>
     */
    private List<Message> messages;

    /**
     * 是否以流式接口的形式返回数据，默认false
     */
    private Boolean stream;

    /**
     * 控制输出随机性的参数
     * <p>
     * 说明：
     * 1. 较高的数值会使输出更加随机，而较低的数值会使其更加集中和确定
     * 2. 默认0.95，范围 (0, 1.0]，不能为0
     * </p>
     */
    private Double temperature;

    /**
     * 控制输出文本多样性的参数
     * <p>
     * 说明：
     * 1. 影响输出文本的多样性，取值越大，生成文本的多样性越强
     * 2. 默认0.7，取值范围 [0, 1.0]
     * </p>
     */
    @SerializedName("top_p")
    private Double topP;

    /**
     * 惩罚分数，用于减少重复生成的现象
     * <p>
     * 说明：
     * 1. 值越大表示惩罚越大
     * 2. 默认1.0，取值范围：[1.0, 2.0]
     * </p>
     */
    @SerializedName("penalty_score")
    private Double penaltyScore;

    /**
     * 模型人设，主要用于人设设定
     * <p>
     * 说明：
     * 1. 长度限制，message中的content总长度和system字段总内容不能超过11200个字符，且不能超过7168 tokens
     * </p>
     */
    private String system;

    /**
     * 生成停止标识
     * <p>
     * 说明：
     * 1. 当模型生成结果以stop中某个元素结尾时，停止文本生成
     * 2. 每个元素长度不超过20字符
     * 3. 最多4个元素
     * </p>
     */
    private List<String> stop;

    /**
     * 指定模型最大输出token数
     * <p>
     * 说明：
     * 1. 如果设置此参数，范围[2, 1024]
     * 2. 如果不设置此参数，最大输出token数为1024
     * </p>
     */
    @SerializedName("max_output_tokens")
    private Integer maxOutputTokens;

    /**
     * 表示最终用户的唯一标识符
     */
    @SerializedName("user_id")
    private String userId;
}
