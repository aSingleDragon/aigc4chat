package pers.lys.aigc4chat.model.baidu;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import pers.lys.aigc4chat.model.baidu.constant.ModelName;
import pers.lys.aigc4chat.model.baidu.request.AgentReq;
import pers.lys.aigc4chat.model.baidu.request.ChatReq;
import pers.lys.aigc4chat.model.baidu.request.body.AgentReqBody;
import pers.lys.aigc4chat.model.baidu.request.body.ChatReqBody;
import pers.lys.aigc4chat.model.baidu.response.body.ChatRespBody;
import pers.lys.aigc4chat.model.baidu.response.body.agent.AgentRespBody;

/**
 * AI模型调用接口
 *
 * @author LiYaosheng
 * @author hll
 * @since 2024/3/26
 */
@Slf4j
@UtilityClass
public class BaiduApi {

    public ChatRespBody chat(ChatReqBody chatReqBody, String modelName) {
        return BaiduHttpClient.post(ChatReq.ofModelName(modelName)
                .setRequestBody(chatReqBody));
    }

    public AgentRespBody agent(AgentReqBody agentReqBody) {
        return BaiduHttpClient.post(new AgentReq(ModelName.EB_INSTANT)
                .setRequestBody(agentReqBody));
    }

    public void refreshAccessToken() {
        BaiduHttpClient.refreshAccessToken();
    }
}
