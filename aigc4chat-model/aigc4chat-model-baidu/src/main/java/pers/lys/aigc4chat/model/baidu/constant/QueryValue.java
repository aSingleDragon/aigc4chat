package pers.lys.aigc4chat.model.baidu.constant;

/**
 * 请求的value
 *
 * @author hll
 * @since 2024/05/05
 */
public final class QueryValue {

    private QueryValue() {}

    public static final String GRANT_TYPE = "client_credentials";
}
