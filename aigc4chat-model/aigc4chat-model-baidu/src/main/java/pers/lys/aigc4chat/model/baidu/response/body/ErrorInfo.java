package pers.lys.aigc4chat.model.baidu.response.body;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

/**
 * 异常信息 <a href="https://cloud.baidu.com/doc/WENXINWORKSHOP/s/tlmyncueh">错误码</a>
 * <pre>{@code
 * {
 *   "error_code" : 336002,
 *   "error_msg" : "Invalid JSON",
 *   "id" : "as-buwugger2p"
 * }
 * }
 * </pre>
 *
 * @author hll
 * @since 2024/05/07
 */
@Data
public class ErrorInfo {

    @SerializedName("error_code")
    private String errorCode;

    @SerializedName("error_msg")
    private String errorMsg;

    /**
     * 本轮对话的id
     */
    private String id;
}
