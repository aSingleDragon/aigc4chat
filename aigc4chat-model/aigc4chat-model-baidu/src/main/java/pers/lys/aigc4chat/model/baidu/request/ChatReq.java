package pers.lys.aigc4chat.model.baidu.request;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
import pers.hll.aigc4chat.base.exception.Assert;
import pers.hll.aigc4chat.base.exception.BizException;
import pers.hll.aigc4chat.base.util.XmlUtil;
import pers.hll.aigc4chat.base.xml.BaiduConfig;
import pers.lys.aigc4chat.model.baidu.request.body.ChatReqBody;
import pers.lys.aigc4chat.model.baidu.response.body.ChatRespBody;

import java.io.IOException;

/**
 * EbInstantReq
 *
 * @author hll
 * @since 2024/05/05
 */
@Slf4j
public class ChatReq extends BasePostRequest<ChatReqBody, ChatRespBody> {

    public ChatReq(String uri) {
        super(uri);
    }

    public static ChatReq ofModelName(String modelName) {
        return new ChatReq(XmlUtil.readXmlConfig(BaiduConfig.class).getChatBaseUrl() + modelName);
    }

    @Override
    public ChatRespBody convertHttpEntityToObj(HttpEntity httpEntity) {
        try {
            ChatRespBody chatRespBody = GSON.fromJson(EntityUtils.toString(httpEntity), ChatRespBody.class);
            Assert.empty(chatRespBody.getErrorCode(),
                    "错误码: {}, 错误描述: {}",
                    chatRespBody.getErrorCode(), chatRespBody.getErrorMsg());
            return chatRespBody;
        } catch (IOException e) {
            log.error("[{}]响应转换异常: ", getUri(), e);
            throw BizException.of("[{}]响应转换异常: ", getUri(), e);
        }
    }
}
