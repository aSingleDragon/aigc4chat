package pers.lys.aigc4chat.model.baidu.gson;

import com.google.gson.*;
import pers.lys.aigc4chat.model.baidu.constant.Role;

import java.lang.reflect.Type;

/**
 * 角色枚举序列化/反序列化器
 *
 * @author hll
 * @since 2024/05/04
 */
public class RoleSerializer implements JsonSerializer<Role>, JsonDeserializer<Role> {
    @Override
    public Role deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {
        return Role.codeOf(json.getAsString());
    }

    @Override
    public JsonElement serialize(Role role, Type type, JsonSerializationContext jsonSerializationContext) {
        return new JsonPrimitive(role.getCode());
    }
}
