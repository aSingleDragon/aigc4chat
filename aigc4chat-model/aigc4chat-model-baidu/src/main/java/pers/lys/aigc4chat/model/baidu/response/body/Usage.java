package pers.lys.aigc4chat.model.baidu.response.body;

import com.google.gson.annotations.SerializedName;
import lombok.Data;


/**
 * 提供问题与回答的tokens数量统计信息的类.
 *
 * @author hll
 * @since 2024/05/05
 */
@Data
public class Usage {

    /**
     * 描述问题部分的tokens数量，即问题的词汇单元个数。
     */
    @SerializedName("prompt_tokens")
    private Integer promptTokens;

    /**
     * 描述回答部分的tokens数量，即回答的词汇单元个数。
     */
    @SerializedName("completion_tokens")
    private Integer completionTokens;

    /**
     * 总tokens数量，即问题和回答词汇单元的总数。
     */
    @SerializedName("total_tokens")
    private Integer totalTokens;
}