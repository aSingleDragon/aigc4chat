package pers.lys.aigc4chat.model.baidu.request;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import pers.lys.aigc4chat.model.baidu.constant.Role;
import pers.lys.aigc4chat.model.baidu.gson.RoleSerializer;

/**
 *
 * @author hll
 * @since 2024/04/30
 */
@Data
@Slf4j
@Accessors(chain = true)
public abstract class BaseRequest<R> {

    protected static final Gson GSON;

    private R responseBody;

    /**
     * 请求资源地址
     */
    private final String uri;

    static {
        GSON = new GsonBuilder()
                .registerTypeAdapter(Role.class, new RoleSerializer())
                .create();
    }

    protected BaseRequest(String uri) {
        this.uri = uri;
    }

    /**
     * 响应体转换成响应对象
     *
     * @param httpEntity 响应体
     * @return 对象 (范型支持类)
     */
    public abstract R convertHttpEntityToObj(HttpEntity httpEntity);
}
