package pers.lys.aigc4chat.model.baidu.response.body.agent;

import lombok.Data;

import java.util.List;

/**
 * 消息
 *
 * @author hll
 * @author lys
 * @since 2024/05/05
 */
@Data
public class Message {

    private List<Answer> answer;
}