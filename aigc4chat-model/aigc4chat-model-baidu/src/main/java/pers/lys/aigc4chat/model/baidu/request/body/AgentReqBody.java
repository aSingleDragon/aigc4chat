package pers.lys.aigc4chat.model.baidu.request.body;

import lombok.Data;

/**
 * Agent自定义对话模型请求参数
 *
 * @author LiYaosheng
 * @since 2024/3/23
 */
@Data
public class AgentReqBody {

    /**
     * 用户问题
     */
    private String queryText;
}
