package pers.lys.aigc4chat.model.baidu.constant;

/**
 * 模型名称
 *
 * @author hll
 * @since 2024/05/06
 */
public final class ModelName {

    private ModelName() {}

    /**
     * <a href="https://cloud.baidu.com/doc/WENXINWORKSHOP/s/4lilb2lpf">ERNIE-Lite-8K-0922</a>
     */
    public static final String EB_INSTANT = "eb-instant";


}
