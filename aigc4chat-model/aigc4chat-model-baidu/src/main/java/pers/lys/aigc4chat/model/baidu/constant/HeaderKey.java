package pers.lys.aigc4chat.model.baidu.constant;

import pers.hll.aigc4chat.base.constant.AuthType;

/**
 * 请求头的key
 *
 * @author hll
 * @since 2024/05/05
 */
public final class HeaderKey {

    private HeaderKey() {
    }

    /**
     * 公共请求头 请求类型
     */
    public static final String CONTENT_TYPE = "Content-Type";

    /**
     * 请求头 当前时间，遵循ISO8601规范，格式如2016-04-06T08:23:49Z {@link AuthType#AK_SK}
     * 当鉴权方式为 {@link AuthType#AK_SK} 使用
     */
    public static final String X_BCE_DATE = "x-bce-date";

    /**
     * 请求头 用于验证请求合法性的认证信息，
     * 更多内容请参考<a href="https://cloud.baidu.com/doc/Reference/s/Njwvz1wot">鉴权认证机制</a>，
     * 签名工具可参考<a href="https://cloud.baidu.com/signature/index.html">IAM签名工具</a>
     * 当鉴权方式为 {@link AuthType#AK_SK} 使用
     */
    public static final String AUTHORIZATION = "Authorization";

    /**
     * 响应头Header 一分钟内允许的最大请求次数
     */
    public static final String X_RATE_LIMIT_LIMIT_REQUESTS = "X-Ratelimit-Limit-Requests";

    /**
     * 响应头Header 一分钟内允许的最大tokens消耗，包含输入tokens和输出tokens
     */
    public static final String X_RATE_LIMIT_LIMIT_TOKENS = "X-Ratelimit-Limit-Tokens";

    /**
     * 响应头Header 达到RPM速率限制前，剩余可发送的请求数配额，如果配额用完，将会在0-60s后刷新
     */
    public static final String X_RATE_LIMIT_REMAINING_REQUESTS = "X-Ratelimit-Remaining-Requests";

    /**
     * 响应头Header 达到TPM速率限制前，剩余可消耗的tokens数配额，如果配额用完，将会在0-60s后刷新
     */
    public static final String X_RATE_LIMIT_REMAINING_TOKENS = "X-Ratelimit-Remaining-Tokens";

    public static final String CACHE_CONTROL = "Cache-Control";

    public static final String CONTENT_DISPOSITION = "Content-Disposition";

    public static final String CONTENT_ENCODING = "Content-Encoding";

    public static final String CONTENT_LENGTH = "Content-Length";

    public static final String CONTENT_MD5 = "Content-MD5";

    public static final String CONTENT_RANGE = "Content-Range";

    public static final String DATE = "Date";

    public static final String ETAG = "ETag";

    public static final String EXPIRES = "Expires";

    public static final String HOST = "Host";

    public static final String LAST_MODIFIED = "Last-Modified";

    public static final String LOCATION = "Location";

    public static final String RANGE = "Range";

    public static final String SERVER = "Server";

    public static final String TRANSFER_ENCODING = "Transfer-Encoding";

    public static final String USER_AGENT = "User-Agent";


    /*
     * BCE Common HTTP Headers
     */

    public static final String BCE_ACL = "x-bce-acl";

    public static final String BCE_ACL_GRANT_READ = "x-bce-grant-read";

    public static final String BCE_ACL_GRANT_FULL_CONTROL = "x-bce-grant-full-control";

    public static final String BCE_CONTENT_SHA256 = "x-bce-content-sha256";

    public static final String BCE_COPY_METADATA_DIRECTIVE = "x-bce-metadata-directive";

    public static final String BCE_COPY_SOURCE_IF_MATCH = "x-bce-copy-source-if-match";

    public static final String BCE_DATE = "x-bce-date";

    public static final String BCE_PREFIX = "x-bce-";

    public static final String BCE_REQUEST_ID = "x-bce-request-id";

    public static final String BCE_SECURITY_TOKEN = "x-bce-security-token";

    public static final String BCE_USER_METADATA_PREFIX = "x-bce-meta-";

    public static final String BCE_CONTENT_CRC32 = "x-bce-content-crc32";

    /*
     * BOS HTTP Headers
     */

    public static final String BCE_COPY_SOURCE = "x-bce-copy-source";

    public static final String BCE_COPY_SOURCE_RANGE = "x-bce-copy-source-range";

    public static final String BCE_COPY_SOURCE_IF_MODIFIED_SINCE = "x-bce-copy-source-if-modified-since";

    public static final String BCE_COPY_SOURCE_IF_NONE_MATCH = "x-bce-copy-source-if-none-match";

    public static final String BCE_COPY_SOURCE_IF_UNMODIFIED_SINCE = "x-bce-copy-source-if-unmodified-since";

    public static final String BCE_FETCH_SOURCE = "x-bce-fetch-source";

    public static final String BCE_FETCH_MODE = "x-bce-fetch-mode";

    public static final String BCE_DEBUG_ID = "x-bce-debug-id";

    public static final String BCE_NEXT_APPEND_OFFSET = "x-bce-next-append-offset";

    public static final String BCE_OBJECT_TYPE = "x-bce-object-type";

    public static final String BCE_STORAGE_CLASS = "x-bce-storage-class";

    public static final String BCE_RESTORE_TIER = "x-bce-restore-tier";

    public static final String BCE_RESTORE_DAYS = "x-bce-restore-days";

    public static final String BCE_SYMLINK_TARGET = "x-bce-symlink-target";

    public static final String BCE_FORBID_OVERWRITE = "x-bce-forbid-overwrite";

    public static final String BCE_RESTORE = "x-bce-restore";

    public static final String BOS_TRAFFIC_LIMIT = "x-bce-traffic-limit";

    public static final String BOS_PROCESS = "x-bce-process";

    public static final String BCE_TAG_LIST = "x-bce-tag-list";

    public static final String BCE_REFERER = "referer";

    public static final String BCE_FETCH_USER_AGENT = "x-bce-fetch-user-agent";

    public static final String BCE_FETCH_CALLBACK_ADDRESS = "x-bce-callback-address";

    public static final String BCE_BUCKET_TYPE = "x-bce-bucket-type";

    public static final String BCE_DELETE_RECURSIVE = "x-bce-delete-recursive";

    public static final String BCE_DELETE_TOKEN = "x-bce-delete-token";

    public static final String BCE_CONTENT_CRC32C = "x-bce-content-crc32c";

    public static final String BCE_CONTENT_CRC32C_FLAG = "x-bce-content-crc32c-flag";

    /*
     * CFC HTTP Headers
     */

    public static final String BCE_LOG_RESULT = "X-Bce-Log-Result";
}
