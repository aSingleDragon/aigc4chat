package pers.lys.aigc4chat.model.baidu.response.body.agent;

import lombok.Data;

/**
 * 回复内容
 *
 * @author hll
 * @author lys
 * @since 2024/05/05
 */
@Data
public class Reply {

    private String text;
}