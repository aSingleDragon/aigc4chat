package pers.lys.aigc4chat.model.baidu.auth;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

/**
 * 获取AccessToken响应消息
 *
 * @author hll
 * @since 2024/05/05
 */
@Data
public class AuthResp {

    /**
     * 访问令牌
     */
    @SerializedName("access_token")
    private String accessToken;

    /**
     * 有效期，Access Token的有效期。
     * 说明：单位是秒，有效期30天
     */
    @SerializedName("expires_in")
    private Integer expiresIn;

    /**
     * 错误信息
     */
    @SerializedName("error_description")
    private String errorDescription;

    /**
     * 错误码
     */
    private String error;

    /**
     * 刷新令牌 百度接口文档说明: 暂时未使用，可忽略
     */
    @SerializedName("refresh_token")
    private String refreshToken;

    /**
     * 会话密钥 百度接口文档说明: 暂时未使用，可忽略
     */
    @SerializedName("session_key")
    private String sessionKey;

    /**
     * 权限范围 百度接口文档说明: 暂时未使用，可忽略
     */
    @SerializedName("scope")
    private String scope;

    /**
     * 会话密钥的密文 百度接口文档说明: 暂时未使用，可忽略
     */
    @SerializedName("session_secret")
    private String sessionSecret;
}
