package pers.lys.aigc4chat.model.baidu.request.body;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pers.lys.aigc4chat.model.baidu.constant.Role;

/**
 * 消息
 *
 * @author hll
 * @since 2024/05/05
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Message {

    private Role role;

    private String content;
}