package pers.lys.aigc4chat.model.baidu.response.body;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**  
 * 用于表示多轮对话的返回结果
 *
 * @author hll
 * @since 2024/05/05
 */  
@Data
@EqualsAndHashCode(callSuper = true)
public class ChatRespBody extends ErrorInfo {

    /**  
     * 回包类型，如 "chat.completion" 表示多轮对话返回  
     */  
    private String object;  
  
    /**  
     * 时间戳  
     */  
    private Long created;
  
    /**  
     * 表示当前子句的序号，仅在流式接口模式下返回  
     */
    @SerializedName("sentence_id")
    private Integer sentenceId;  
  
    /**  
     * 表示当前子句是否是最后一句，仅在流式接口模式下返回  
     */
    @SerializedName("is_end")
    private Boolean isEnd;  
  
    /**  
     * 当前生成的结果是否被截断  
     */
    @SerializedName("is_truncated")
    private Boolean isTruncated;  
  
    /**  
     * 对话返回结果  
     */  
    private String result;  
  
    /**  
     * 表示用户输入是否存在安全风险，是否关闭当前会话，清理历史会话信息  
     * true：是，表示用户输入存在安全风险，建议关闭当前会话，清理历史会话信息  
     * false：否，表示用户输入无安全风险  
     */
    @SerializedName("need_clear_history")
    private Boolean needClearHistory;  
  
    /**  
     * 当need_clear_history为true时，此字段会告知第几轮对话有敏感信息  
     * 如果是当前问题，ban_round=-1  
     */
    @SerializedName("ban_round")
    private Integer banRound;  
  
    /**  
     * token统计信息  
     */  
    private Usage usage;
}