package pers.lys.aigc4chat.model.baidu.constant;

/**
 * 请求的key
 *
 * @author hll
 * @since 2024/05/05
 */
public final class QueryKey {

    private QueryKey() {}

    public static final String GRANT_TYPE = "grant_type";

    public static final String CLIENT_ID = "client_id";

    public static final String CLIENT_SECRET = "client_secret";

    public static final String ACCESS_TOKEN = "access_token";
}
