package pers.lys.aigc4chat.model.baidu.response.body.agent;

import lombok.Data;

/**
 * Agent自定义对话模型响应参数
 *
 * @author LiYaosheng
 * @author hll
 * @since 2024/3/23
 */
@Data
public class AgentRespBody {

    private Message data;
}
