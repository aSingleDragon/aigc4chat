package pers.lys.aigc4chat.model.baidu.request;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
import pers.hll.aigc4chat.base.exception.BizException;
import pers.lys.aigc4chat.model.baidu.request.body.AgentReqBody;
import pers.lys.aigc4chat.model.baidu.response.body.agent.AgentRespBody;

import java.io.IOException;

/**
 * AgentReq
 *
 * @author hll
 * @since 2024/05/05
 */
@Slf4j
public class AgentReq extends BasePostRequest<AgentReqBody, AgentRespBody> {

    public AgentReq(String uri) {
        super(uri);
    }

    @Override
    public AgentRespBody convertHttpEntityToObj(HttpEntity httpEntity) {
        try {
            return GSON.fromJson(EntityUtils.toString(httpEntity), AgentRespBody.class);
        } catch (IOException e) {
            log.error("[{}]响应转换异常: ", getUri(), e);
            throw BizException.of("[{}]响应转换异常: ", getUri(), e);
        }
    }
}
