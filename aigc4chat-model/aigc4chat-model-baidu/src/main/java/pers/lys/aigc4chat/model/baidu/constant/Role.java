package pers.lys.aigc4chat.model.baidu.constant;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 角色
 *
 * @author hll
 * @see pers.lys.aigc4chat.model.baidu.request.body.Message
 * @since 2024/05/05
 */
@Getter
@RequiredArgsConstructor
public enum Role {

    /**
     * 用户
     */
    USER("user"),

    /**
     * 对话助手
     */
    ASSISTANT("assistant");

    private final String code;

    public static Role codeOf(String code) {
        for (Role role : Role.values()) {
            if (role.getCode().equals(code)) {
                return role;
            }
        }
        return USER;
    }
}
