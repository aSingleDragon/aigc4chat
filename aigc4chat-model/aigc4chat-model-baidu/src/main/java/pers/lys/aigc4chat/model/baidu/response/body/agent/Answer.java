package pers.lys.aigc4chat.model.baidu.response.body.agent;

import lombok.Data;

/**
 * 问题
 *
 * @author hll
 * @author lys
 * @since 2024/05/05
 */
@Data
public class Answer {

    private Reply reply;
}