- [百度智能云在线服务](https://console.bce.baidu.com/qianfan/ais/console/onlineService?tab=preset)
- [ERNIE-Lite-8K-0922 接口文档](https://cloud.baidu.com/doc/WENXINWORKSHOP/s/4lilb2lpf)

# API调用
1. 开通大模型接口使用权限 [大模型计费管理](https://console.bce.baidu.com/qianfan/chargemanage/list)
2. 创建应用 [应用接入](https://console.bce.baidu.com/qianfan/ais/console/applicationConsole/application)
3. 获得APIKey 和 SecretKey 并写入百度的XML配置文件