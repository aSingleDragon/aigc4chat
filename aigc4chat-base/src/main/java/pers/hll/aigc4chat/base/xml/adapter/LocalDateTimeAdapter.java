package pers.hll.aigc4chat.base.xml.adapter;

import jakarta.xml.bind.annotation.adapters.XmlAdapter;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;  
import java.time.format.DateTimeFormatter;

/**
 * LocalDateTimeAdapter
 *
 * @author hll
 * @since 2024/05/07
 */
public class LocalDateTimeAdapter extends XmlAdapter<String, LocalDateTime> {  

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Override  
    public LocalDateTime unmarshal(String v) {
        if (StringUtils.isBlank(v)) {
            return null;
        }
        return LocalDateTime.parse(v, FORMATTER);
    }  

    @Override  
    public String marshal(LocalDateTime v) {
        return v == null ? null : FORMATTER.format(v);
    }  
}