package pers.hll.aigc4chat.base.constant;

/**
 * 鉴权类型
 *
 * @author hll
 * @since 2024/05/05
 */
public enum AuthType {

    ACCESS_TOKEN,

    AK_SK;
}
