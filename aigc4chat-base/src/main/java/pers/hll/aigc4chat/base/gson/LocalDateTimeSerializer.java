package pers.hll.aigc4chat.base.gson;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * Gson的LocalDateTime反序列化器
 *
 * @author hll
 * @since 2024/04/30
 */
public class LocalDateTimeSerializer implements JsonDeserializer<LocalDateTime>, JsonSerializer<LocalDateTime> {

    @Override  
    public LocalDateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {  
        String dateTimeString = json.getAsString();
        Instant instant = Instant.parse(dateTimeString);
        return LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
    }

    @Override
    public JsonElement serialize(LocalDateTime localDateTime, Type type, JsonSerializationContext context) {
        return new JsonPrimitive(localDateTime.toString());
    }
}