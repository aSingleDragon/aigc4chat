package pers.hll.aigc4chat.base.exception;

import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;

import java.util.function.Predicate;

/**
 * 断言
 *
 * @author hll
 * @since 2024/05/07
 */
@UtilityClass
public class Assert {

    public void notEmpty(String str, String message) {
        if (StringUtils.isEmpty(str)) {
            throw BizException.of(message);
        }
    }

    public void empty(String str, String pattern, Object... arguments) {
        if (StringUtils.isNotEmpty(str)) {
            throw BizException.of(pattern, arguments);
        }
    }

    public void allNotEmpty(String... args) {
        for (String str : args) {
            notEmpty(str, "");
        }
    }

    /**
     * 根据Predicate条件判断, 不满足条件则抛出异常
     *
     * @param predicate Predicate 条件
     * @param obj 对象
     * @param pattern 异常信息模式
     * @param arguments 异常信息模式的参数列表 如果有{@link Throwable}的参数, 请放在参数列表最后一个位置
     */
    public <T> void notTest(Predicate<T> predicate, T obj, String pattern, Object... arguments) {
        if (!predicate.test(obj)) {
            throw BizException.of(pattern, arguments);
        }
    }
}
