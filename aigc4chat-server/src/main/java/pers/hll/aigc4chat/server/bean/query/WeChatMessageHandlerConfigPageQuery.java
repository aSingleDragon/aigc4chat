package pers.hll.aigc4chat.server.bean.query;

import io.swagger.v3.oas.annotations.Parameter;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 消息处理器配置分页查询参数
 *
 * @author hll
 * @since 2024/05/12
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WeChatMessageHandlerConfigPageQuery extends PageQuery {

    private String remarkName;

    private String handlerName;
}
