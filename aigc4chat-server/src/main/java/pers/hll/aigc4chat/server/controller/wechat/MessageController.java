package pers.hll.aigc4chat.server.controller.wechat;


import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pers.hll.aigc4chat.base.util.MultipartFileUtil;
import pers.hll.aigc4chat.entity.wechat.message.OriContent;
import pers.hll.aigc4chat.protocol.wechat.response.LoginResp;
import pers.hll.aigc4chat.server.base.R;
import pers.hll.aigc4chat.server.bean.query.WeChatMessagePageQuery;
import pers.hll.aigc4chat.server.entity.WeChatMessage;
import pers.hll.aigc4chat.server.service.IWeChatApiService;
import pers.hll.aigc4chat.server.service.IWeChatMessageService;
import pers.lys.aigc4chat.model.baidu.constant.Role;
import pers.lys.aigc4chat.model.baidu.request.body.Message;

import java.io.IOException;
import java.util.List;
import java.util.function.Function;

/**
 * 消息控制器
 *
 * @author hll
 * @since 2024/04/14
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/message")
@Tag(name = "MessageController", description = "消息控制器")
public class MessageController {

    private final IWeChatMessageService weChatMessageService;

    private final IWeChatApiService weChatApiService;

    @GetMapping("/page")
    @Operation(summary = "消息-查询-分页", description = "默认分页参数: {\"pageSize\" = 1 , \"pageNum\" = 10}\"")
    public R<IPage<WeChatMessage>> pageMessage(WeChatMessagePageQuery query) {
        return R.data(weChatMessageService.pageMessage(query));
    }

    @PostMapping("/send/text")
    @Operation(summary = "发送文字消息")
    public R<LoginResp> sendText(@RequestParam @Parameter(description = "要发送的文字内容") String text,
                                 @RequestParam @Parameter(description = "要发送到的用户名") String toUserName) {
        weChatApiService.activeCheck();
        weChatApiService.sendTextMessage(text, toUserName);
        return R.success();
    }

    @PostMapping("/send/location")
    @Operation(summary = "发送地理位置消息")
    public R<LoginResp> sendLocation(@RequestBody OriContent location,
                                     @RequestParam @Parameter(description = "要发送到的用户名") String toUserName) {
        weChatApiService.activeCheck();
        weChatApiService.sendLocationMessage(location, toUserName);
        return R.success();
    }

    @Operation(summary = "发送文件消息")
    @PostMapping(value = "/send/file", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public R<LoginResp> sendFile(@RequestPart("file") @Parameter(description = "要发送到的文件") MultipartFile file,
                                 @RequestParam @Parameter(description = "要发送到的用户名") String toUserName)
            throws IOException {
        weChatApiService.activeCheck();
        String filePath = MultipartFileUtil.saveFile(file);
        weChatApiService.sendFileMessage(filePath, toUserName);
        return R.success(filePath);
    }

    @Operation(summary = "获得文字聊天记录")
    @GetMapping("/chat-history")
    public R<List<Message>> chatHistory(
            @RequestParam @Parameter(description = "用户名") String userName,
            @RequestParam(defaultValue = "10") @Parameter(description = "聊天记录条数") Integer count) {

        List<WeChatMessage> weChatMessages = weChatMessageService.listChatHistoryForModel(userName, count);

        Function<WeChatMessage, Message> messageFunction = x -> new Message(
                StringUtils.equals(x.getFromUserName(), userName) ? Role.USER : Role.ASSISTANT,
                x.getContent());

        return R.data(weChatMessages.stream()
                .map(messageFunction)
                .toList());
    }
}
