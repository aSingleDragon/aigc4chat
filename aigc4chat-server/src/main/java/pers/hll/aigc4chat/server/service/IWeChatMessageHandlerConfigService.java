package pers.hll.aigc4chat.server.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import pers.hll.aigc4chat.server.bean.query.WeChatMessageHandlerConfigPageQuery;
import pers.hll.aigc4chat.server.entity.WeChatMessageHandlerConfig;
import com.baomidou.mybatisplus.extension.service.IService;
import pers.hll.aigc4chat.server.handler.MessageHandler;

import java.util.List;

/**
 * <p>
 *  消息处理器服务接口
 * </p>
 *
 * @author hll
 * @since 2024-04-22
 */
public interface IWeChatMessageHandlerConfigService extends IService<WeChatMessageHandlerConfig> {

    /**
     * 根据备注名/昵称获取处理器名称
     *
     * @param remarkName 备注
     * @param nickName 昵称
     * @return 处理器名称
     */
    String getHandlerName(String remarkName, String nickName);

    List<String> listHandlerName();

    MessageHandler getMessageHandler(String beanName);

    IPage<WeChatMessageHandlerConfig> page(WeChatMessageHandlerConfigPageQuery query);
}
