package pers.hll.aigc4chat.server.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import pers.hll.aigc4chat.server.bean.query.WeChatMessagePageQuery;
import pers.hll.aigc4chat.server.entity.WeChatMessage;

import java.util.List;

/**
 * <p>
 * 消息 接口
 * </p>
 *
 * @author hll
 * @since 2024/03/31
 */
public interface IWeChatMessageService extends IService<WeChatMessage> {

    IPage<WeChatMessage> pageMessage(WeChatMessagePageQuery query);

    /**
     * 获取和 {@code userName}的聊天记录 在调用大模型接口时使用
     * <p>注意: 微信的聊天记录和大模型要求的历史对话的格式有一定的区别
     * 大模型的对话记录要求如下:
     * <ol>
     *   <li>messages成员不能为空，1个成员表示单轮对话，多个成员表示多轮对话</li>
     *   <li>最后一个message为当前请求的信息，前面的message为历史对话信息</li>
     *   <li>必须为奇数个成员，成员中message的role必须依次为user、assistant</li>
     * </ol>
     * 这里会将连续同一用户发送的消息合并为一条
     *
     * @param userName 用户名
     * @param count 聊天记录条数
     * @return 聊天记录
     */
    List<WeChatMessage> listChatHistoryForModel(String userName, Integer count);

    /**
     * 获取和 {@code userName}的文字聊天记录
     *
     * @param userName 用户名
     * @return 文字聊天记录
     */
    List<WeChatMessage> listTextChatHistory(String userName, Integer count);
}
