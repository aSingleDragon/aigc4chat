package pers.hll.aigc4chat.server.converter;

import pers.hll.aigc4chat.base.util.converter.ConverterUtil;
import pers.hll.aigc4chat.server.bean.command.BaiduConfigCommand;
import pers.hll.aigc4chat.base.xml.BaiduConfig;

import lombok.experimental.UtilityClass;

import java.util.Collections;
import java.util.List;

/**
* 实体类转换Converter
* <p> 由 {@link ConverterUtil} 生成
*
* @author hll
* @since 2024-05-07
*/
@UtilityClass
public class BaiduConfigConverter {

    public BaiduConfig from(BaiduConfigCommand from) {
        if (from == null) {
            return null;
        }
        BaiduConfig to = new BaiduConfig();
        to.setAuthType(from.getAuthType());
        to.setAccessKeyId(from.getAccessKeyId());
        to.setSecretKey(from.getSecretKey());
        to.setApiKey(from.getApiKey());
        to.setSecretAccessKey(from.getSecretAccessKey());
        return to;
    }

    public List<BaiduConfig> from(List<BaiduConfigCommand> fromList) {
        if (fromList == null) {
            return Collections.emptyList();
        }
        return fromList
                .stream()
                .map(BaiduConfigConverter::from)
                .toList();
    }
}