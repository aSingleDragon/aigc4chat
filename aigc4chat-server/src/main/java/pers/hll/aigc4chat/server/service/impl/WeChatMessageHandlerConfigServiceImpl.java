package pers.hll.aigc4chat.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationContext;
import pers.hll.aigc4chat.base.util.ConstantUtil;
import pers.hll.aigc4chat.server.bean.query.WeChatMessageHandlerConfigPageQuery;
import pers.hll.aigc4chat.server.entity.WeChatMessageHandlerConfig;
import pers.hll.aigc4chat.server.entity.WeChatUser;
import pers.hll.aigc4chat.server.handler.MessageHandler;
import pers.hll.aigc4chat.server.handler.MessageHandlerName;
import pers.hll.aigc4chat.server.mapper.WechatMessageHandlerConfigMapper;
import pers.hll.aigc4chat.server.service.IWeChatMessageHandlerConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import pers.hll.aigc4chat.server.service.IWeChatUserService;
import pers.hll.aigc4chat.server.util.PageUtil;

import java.util.List;

import static org.apache.commons.lang3.StringUtils.*;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author hll
 * @since 2024-04-22
 */
@Service
@RequiredArgsConstructor
public class WeChatMessageHandlerConfigServiceImpl
        extends ServiceImpl<WechatMessageHandlerConfigMapper, WeChatMessageHandlerConfig>
        implements IWeChatMessageHandlerConfigService {

    private final ApplicationContext applicationContext;

    @Override
    public String getHandlerName(String remarkName, String nickName) {
        return lambdaQuery()
                .eq(WeChatMessageHandlerConfig::getRemarkName, remarkName)
                .eq(WeChatMessageHandlerConfig::getNickName, nickName)
                .oneOpt()
                .map(WeChatMessageHandlerConfig::getHandlerName)
                .orElse(null);
    }

    @Override
    public List<String> listHandlerName() {
        return ConstantUtil.listStringConstant(MessageHandlerName.class);
    }

    @Override
    public MessageHandler getMessageHandler(String beanName) {
        if (isEmpty(beanName)) {
            return (MessageHandler) applicationContext.getBean(MessageHandlerName.DEFAULT_MESSAGE_HANDLER);
        }
        return (MessageHandler) applicationContext.getBean(beanName);
    }

    @Override
    public IPage<WeChatMessageHandlerConfig> page(WeChatMessageHandlerConfigPageQuery q) {
        return page(PageUtil.createPage(q), new LambdaQueryWrapper<WeChatMessageHandlerConfig>()
                .eq(isNotEmpty(q.getRemarkName()), WeChatMessageHandlerConfig::getRemarkName, q.getRemarkName())
                .eq(isNotEmpty(q.getHandlerName()), WeChatMessageHandlerConfig::getHandlerName, q.getHandlerName()));
    }
}
