package pers.hll.aigc4chat.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import pers.hll.aigc4chat.protocol.wechat.constant.MsgType;
import pers.hll.aigc4chat.server.bean.query.WeChatMessagePageQuery;
import pers.hll.aigc4chat.server.entity.BaseEntity;
import pers.hll.aigc4chat.server.entity.WeChatMessage;
import pers.hll.aigc4chat.server.mapper.WeChatMessageMapper;
import pers.hll.aigc4chat.server.mapper.WeChatUserMapper;
import pers.hll.aigc4chat.server.service.IWeChatMessageService;
import pers.hll.aigc4chat.server.util.PageUtil;

import java.util.LinkedList;
import java.util.List;

/**
 * <p>
 * 消息 服务实现类
 * </p>
 *
 * @author hll
 * @since 2024-04-14
 */
@Service
@RequiredArgsConstructor
public class WeChatMessageServiceImpl extends ServiceImpl<WeChatMessageMapper, WeChatMessage>
        implements IWeChatMessageService {

    private final WeChatMessageMapper weChatMessageMapper;

    @Override
    public IPage<WeChatMessage> pageMessage(WeChatMessagePageQuery query) {
        IPage<WeChatMessage> page = PageUtil.createPage(query);
        return weChatMessageMapper.selectPage(page, new LambdaQueryWrapper<WeChatMessage>()
                .eq(StringUtils.isNotEmpty(query.getFormUserName()), WeChatMessage::getFromUserName, query.getFormUserName())
                .eq(StringUtils.isNotEmpty(query.getToUserName()), WeChatMessage::getToUserName, query.getToUserName())
                .orderByAsc(BaseEntity::getUpdatedTime));
    }

    @Override
    public List<WeChatMessage> listChatHistoryForModel(String userName, Integer count) {
        List<WeChatMessage> messageList = listTextChatHistory(userName, 50);
        LinkedList<WeChatMessage> chatList = new LinkedList<>();
        // 将同一用户连续发送的消息合并为一条消息
        for (WeChatMessage weChatMessage : messageList) {
            if (chatList.peek() == null) {
                if (StringUtils.equals(weChatMessage.getFromUserName(), userName)) {
                    chatList.push(weChatMessage);
                }
            } else {
                WeChatMessage peek = chatList.peek();
                if (StringUtils.equals(peek.getFromUserName(), weChatMessage.getFromUserName())) {
                    peek.setContent(weChatMessage.getContent() + "。" + peek.getContent());
                } else {
                    chatList.push(weChatMessage);
                }
            }
        }
        if (chatList.size() <= 1) {
            return chatList;
        }
        // 确保为奇数条消息
        count = Math.min(count, chatList.size());
        if ((count & 1) == 0) {
            chatList.pop();
        }
        return chatList;
    }

    @Override
    public List<WeChatMessage> listTextChatHistory(String userName, Integer count) {
        return lambdaQuery()
                .eq(WeChatMessage::getMsgType, MsgType.TEXT)
                .eq(WeChatMessage::getSubMsgType, 0)
                .and(e -> e.eq(WeChatMessage::getFromUserName, userName)
                        .or()
                        .eq(WeChatMessage::getToUserName, userName))
                .orderByDesc(BaseEntity::getCreatedTime)
                .list();
    }
}
