package pers.hll.aigc4chat.server.controller.wechat;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;
import pers.hll.aigc4chat.base.exception.BizException;
import pers.hll.aigc4chat.server.base.R;
import pers.hll.aigc4chat.server.bean.query.WeChatMessageHandlerConfigPageQuery;
import pers.hll.aigc4chat.server.entity.WeChatMessageHandlerConfig;
import pers.hll.aigc4chat.server.entity.WeChatUser;
import pers.hll.aigc4chat.server.service.IWeChatUserService;
import pers.hll.aigc4chat.server.service.IWeChatMessageHandlerConfigService;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author hll
 * @since 2024-04-22
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/message-handler")
@Tag(name = "WechatMessageHandlerConfigController", description = "消息处理器控制器")
public class WechatMessageHandlerConfigController {

    private final IWeChatMessageHandlerConfigService weChatMessageHandlerConfigService;

    private final IWeChatUserService weChatUserService;

    @GetMapping("/page")
    @Operation(summary = "消息处理器-查询-分页", description = "默认分页参数: {\"pageSize\" = 1 , \"pageNum\" = 10}\"")
    public R<IPage<WeChatMessageHandlerConfig>> page(WeChatMessageHandlerConfigPageQuery query) {
        return R.data(weChatMessageHandlerConfigService.page(query));
    }

    @GetMapping("/list/handler-name")
    @Operation(summary = "处理器名称-列表", description = "获得所有处理器的名称")
    public R<List<String>> listHandlerName() {
        return R.data(weChatMessageHandlerConfigService.listHandlerName());
    }

    @PostMapping("/save-or-update")
    @Operation(summary = "新增/更新用户的消息处理器", description = "增加或更新某用户对应的消息处理器")
    public R<String> saveOrUpdate(
            @RequestParam @Parameter(description = "用户名(请保证用户的备注/非空且唯一)") String name,
            @RequestParam @Parameter(description = "处理器名称") String handlerName) {

        if (!weChatMessageHandlerConfigService.listHandlerName().contains(handlerName)) {
            throw BizException.of("未找到[{}]对应的消息处理器!", handlerName);
        }
        WeChatUser user = weChatUserService.getOneBydName(name);
        String oldHandlerName =
                weChatMessageHandlerConfigService.getHandlerName(user.getRemarkName(), user.getNickName());
        if (StringUtils.isNotEmpty(oldHandlerName)) {
            weChatMessageHandlerConfigService.remove(new LambdaQueryWrapper<WeChatMessageHandlerConfig>()
                    .eq(WeChatMessageHandlerConfig::getRemarkName, user.getRemarkName())
                    .eq(WeChatMessageHandlerConfig::getNickName, user.getNickName()));
        }
        weChatMessageHandlerConfigService.save(WeChatMessageHandlerConfig
                .builder()
                .remarkName(user.getRemarkName())
                .nickName(user.getNickName())
                .handlerName(handlerName)
                .build());
        return R.success();
    }

    @GetMapping("/get-handler-name")
    @Operation(summary = "查询用户的消息处理器")
    public R<String> getHandlerName(@RequestParam @Parameter(description = "用户名称") String name) {
        WeChatUser user = weChatUserService.getOneBydName(name);
        return R.data(weChatMessageHandlerConfigService.getHandlerName(user.getRemarkName(), user.getNickName()));
    }

    @PostMapping("/delete")
    @Operation(summary = "删除用户的消息处理器")
    public R<String> delete(@RequestParam @Parameter(description = "用户名称") String name) {
        WeChatUser user = weChatUserService.getOneBydName(name);
        weChatMessageHandlerConfigService.remove(new LambdaQueryWrapper<WeChatMessageHandlerConfig>()
                .eq(WeChatMessageHandlerConfig::getRemarkName, user.getRemarkName())
                .eq(WeChatMessageHandlerConfig::getNickName, user.getNickName()));
        return R.success();
    }
}
