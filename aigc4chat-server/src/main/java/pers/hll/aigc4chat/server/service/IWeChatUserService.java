package pers.hll.aigc4chat.server.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import pers.hll.aigc4chat.server.bean.query.WeChatUserPageQuery;
import pers.hll.aigc4chat.server.entity.WeChatUser;

import java.util.List;

/**
 * <p>
 * 用户 接口
 * </p>
 *
 * @author hll
 * @since 2024/03/31
 */
public interface IWeChatUserService extends IService<WeChatUser> {

    void saveOrUpdateMe(WeChatUser me);

    WeChatUser selectMe();

    IPage<WeChatUser> pageGroup(WeChatUserPageQuery query);

    IPage<WeChatUser> pageFriend(WeChatUserPageQuery query);

    /**
     * 模糊查询 用户名｜备注｜昵称 与 {@code name} 相似的用户
     *
     * @param name 名称
     * @return 用户列表
     */
    List<WeChatUser> listByName(String name);

    WeChatUser getOneBydName(String name);
}
