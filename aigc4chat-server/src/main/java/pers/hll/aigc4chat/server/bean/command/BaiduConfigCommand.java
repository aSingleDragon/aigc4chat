package pers.hll.aigc4chat.server.bean.command;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import pers.hll.aigc4chat.base.constant.AuthType;

/**
 * 百度配置更新命令
 *
 * @author hll
 * @since 2024/05/07
 * @see pers.hll.aigc4chat.base.xml.BaiduConfig
 */
@Data
public class BaiduConfigCommand {

    @NotNull(message = "鉴权类型应为[AK_SK]或[ACCESS_TOKEN], 不能为空或其他类型！")
    private AuthType authType;

    private String accessKeyId;

    private String secretAccessKey;

    private String apiKey;

    private String secretKey;
}
