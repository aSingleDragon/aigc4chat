# 需求-Story
- ChatGPT接入
- Gemini接入
- Midjourney接入
- 星火接入
- 通艺千问接入

# 缺陷-Bug
- 重复登录有时后台会没有响应
- 轮询获取消息的线程有时不按设定的频率请求数据
- 调用lamma3-chinese接口乱码

# 优化-Improvement
- 回复群消息时 没有对是否是@自己的消息进行区分 没有对谁@自己进行区分
- 调用大模型时 更多的参数配置

# 重构-Refactor